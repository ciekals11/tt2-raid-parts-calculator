# Tap Titans 2 raid part attack calculator

[![pipeline](https://gitlab.com/ciekals11/tt2-raid-parts-calculator/badges/main/pipeline.svg?ignore_skipped=true)](https://gitlab.com/ciekals11/tt2-raid-parts-calculator)

1) Open `index.html` file in your browser
2) Provide all required info.
3) Click `Calculate`.
4) Under the `Calculate` button, a box will appear with information on which parts to attack. 
