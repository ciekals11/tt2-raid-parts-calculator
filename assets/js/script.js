"use strict";

const parts = [
    'head',
    'torso',
    'arm1',
    'arm2',
    'leg1',
    'leg2',
];

const getSubsets = mainSet => {
    const subsets = [[]];

    for (let i = 0; i < mainSet.length; i++) {
        const currentSubsetCount = subsets.length;
        for (let j = 0; j < currentSubsetCount; j++) {
            const currentSubset = subsets[j];
            const newSubset = currentSubset.concat(mainSet[i]);
            subsets.push(newSubset);
        }
    }

    return subsets.filter(subset => subset !== undefined && subset.length >= 1);
};

const removeSubsetsThatDoNotAddUp = (subsets, currParts) => {
    return subsets.filter(item => {
        item = item.map(part => part.replace('1', 's').replace('2', 's'));

        let sum = 0;
        item.forEach(part => {
            sum += currParts.parts[part];
        });

        return sum >= currParts.total;
    });
};

const calcBestWithArmor = (parts, values) => {
    let bestParts = null;
    let lowestTotalHp = Infinity;

    parts.forEach(currParts => {
        let currentTotalHp = 0;
        currParts.forEach(part => {
            const partName = part.replace('1', 's').replace('2', 's')
            currentTotalHp += values.hp.parts[partName];
            currentTotalHp += values.armor.parts[partName];
        });

        if (currentTotalHp < lowestTotalHp) {
            bestParts = currParts;
            lowestTotalHp = currentTotalHp;
        }
    });

    return bestParts;
};

const calculatePartsToAttack = (values) => {
    let subsets = getSubsets(parts);
    subsets = removeSubsetsThatDoNotAddUp(subsets, values.hp);
    subsets = subsets.sort((a, b) => a.length - b.length);

    const resultElement = document.getElementById('result');
    const bestWithArmor = calcBestWithArmor(subsets, values);

    if (bestWithArmor === null) {
        resultElement.querySelector('.result-info').innerHTML = 'Cannot calculate';
        resultElement.classList.remove('hidden');
        return;
    }

    resultElement.querySelector('.result-info').innerHTML = calcBestWithArmor(subsets, values).join(', ');
    resultElement.classList.remove('hidden');
};

const getFormValue = (fieldId, check = true) => {
    const val = document.getElementById(fieldId).value ?? '';

    if (check === false) {
        return val;
    }

    if (val === '') {
        return 0.0;
    }

    return parseFloat(document.getElementById(fieldId).value);
};

const init = () => {
    document.getElementById('part-calculator').addEventListener('submit', e => {
        e.preventDefault();
        const values = {
            hp: {
                parts: {
                    head: getFormValue('head_hp'),
                    torso: getFormValue('torso_hp'),
                    arms: getFormValue('arms_hp') / 2,
                    legs: getFormValue('legs_hp') / 2,
                },
                total: getFormValue('total_hp'),
            },
            armor: {
                parts: {
                    head: getFormValue('head_armor'),
                    torso: getFormValue('torso_armor'),
                    arms: getFormValue('arms_armor') / 2,
                    legs: getFormValue('legs_armor') / 2,
                },
            },
        };

        calculatePartsToAttack(values);
    });
};

document.addEventListener('readystatechange', () => {
    if (document.readyState === 'complete') {
        init();
    }
});
